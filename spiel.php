<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css.php">
        <title>Schach</title>
    </head>
    
    <body>
        <header> 
        <a href="spiel.php">Neuses Spiel beginnen</a>
        
         <?php
        function schachregeln ($_zugnr, $_vonZeile, $_vonSpalte, $nach_Zeile, $nach_Spalte)
        {
            if ($_vonZeile == $_nachZeile && $_vonSpalte == $_nachSpalte)
            {return false;}
            return true;
        }
        ?>
        <div>
            <table class="Schach">
                <?php 
                
                $RookW     = '&#9814;';
                $KnightW   = '&#9816;';
                $BishopW   = '&#9815;';
                $KingW     = '&#9812;';
                $QueenW    = '&#9813;';
                $PawnW     = '&#9817;';
                $RookB     = '&#9820;';
                $KnightB   = '&#9822;';
                $BishopB   = '&#9821;';
                $KingB     = '&#9818;';
                $QueenB    = '&#9819;';
                $PawnB     = '&#9823;';
                
            $zugnr = 1;
            $playercolor = "Orange";
            
            $schachfeld = array (
                array('&#92;','A','B','C','D','E','F','G','H','&#47;'),
				array('8',$RookB,$KnightB,$BishopB,$QueenB,$KingB,$BishopB,$KnightB,$RookB,'8'),
				array('7',$PawnB,$PawnB,$PawnB,$PawnB,$PawnB,$PawnB,$PawnB,$PawnB,'7'),
				array('6','','','','','','','','','6'),
				array('5','','','','','','','','','5'),
				array('4','','','','','','','','','4'),
				array('3','','','','','','','','','3'),
				array('2',$PawnW,$PawnW,$PawnW,$PawnW,$PawnW,$PawnW,$PawnW,$PawnW,'2'),
				array('1',$RookW,$KnightW,$BishopW,$QueenW,$KingW,$BishopW,$KnightW,$RookW,'1'),
				array('&#47;','A','B','C','D','E','F','G','H','&#92;'),
				);
                
            $spielstand = $schachfeld;
            
            if (isset ($_GET['spielstand'])) {
					$schachfeld = unserialize(urldecode($_GET['spielstand']));
					}
				
				if (isset ($_GET['zugnummer']))						
				{
				    if(file_exists('spielstand.txt'))
						 	{
							$handle = fopen("spielstand.txt","r");
							$spielstand=unserialize(base64_decode(fread($handle,filesize("spielstand.txt"))));
							fclose($handle);
							}
								else
							{
								echo "Datei nicht gefunden";
							}
					$zugnr = $_GET['zugnummer'];
				
				if ($zugnr % 2)
				{ $playercolor = "Grün"; }
				else
				{ $playercolor = "Orange"; }
			}	
              if (isset ($_GET['Von']) && isset ($_GET['Nach']))
              { $vonZeile = 9-(ord(substr($_GET['Von'],1,1))-48);
                $vonSpalte = (ord(substr($_GET['Von'],0,1))-64);	
                $nachZeile = 9-(ord(substr($_GET['Nach'],1,1))-48);
                $nachSpalte = (ord(substr($_GET['Nach'],0,1))-64);
                
                if (schachregeln($zugnr, $vonZeile, $vonSpalte, $nachZeile, $nachSpalte))
                { $zugnr++;
                    echo "Dies ist der ".$zugnr.". Zug, somit ist $playercolor an der Reihe.";
                    $schachfeld[$nachZeile][$nachSpalte] = $schachfeld[$vonZeile][$vonSpalte];
                    $schachfeld[$vonZeile][$vonSpalte] = '';
                    
                $spielstand = $schachfeld;
                }
             }   
            else {echo "Bitte machen Sie den $zugnr. Zug, $playercolor. </p>";
                $spielstand = $schachfeld;
                 }
            foreach($schachfeld as $zeile)
              { echo "<tr>";
                foreach($zeile as $zelle)
                    { if ($zelle == $RookW || $zelle == $KnightW || $zelle == $BishopW || $zelle == $KingW || $zelle == $QueenW || $zelle == $PawnW)
                     { echo "<td class = 'white'> $zelle </td>"; }
                     
                     else if ($zelle == $RookB || $zelle == $KnightB || $zelle == $BishopB || $zelle == $KingB || $zelle == $QueenB || $zelle == $PawnB)
                     { echo "<td class = 'black'> $zelle </td>"; }
                     
                     else { echo "<td>$zelle</td>"; }
                    }
                    
                echo "</tr></div>";
                $spielstatus =base64_encode(serialize($spielstand));
                $handle=fopen("spielstand.txt", "w");
                fwrite($handle, $spielstatus);
                fclose($handle);
                unset($handle);
              }
              
         ?>
         <form action ="" method="GET">
             
        <?php ?>
        <div class="Von_Nach">
            <label><span>Von - Nach</span><br></label> 
                <select name="Von" size="4" title="Von" multiple>
                    <?php
                        for($i='A'; $i<='H'; $i++)
                            { for($j='1'; $j<='8'; $j++)
                                { if(!empty($spielstand[9-$j][ord($i)-64]))
                                    { echo "<option value=".$i.$j.">".$i.$j."</option>";
                                    }
                                }
                            }
                    ?>        
                </select>
                
        <?php ?>
            <label> </label>
            <select name="Nach" size="4" title="Nach">
        <?php
            for ($i='A'; $i<='H'; $i++)
                { for ($j='1'; $j<='8'; $j++)
                    { if(empty($spielstand[9-$j][ord($i)-64]))
                        {echo "<option value=".$i.$j.">".$i.$j."</option>";
                        }
                    }
                }
                
            echo "</select><br><input type='hidden' name='zugnummer' value='".$zugnr."'>";
            echo "<input type='hidden' name='spielstand' value='" .urlencode(serialize($spielstand))."'>";
            echo "<input type='submit' value='Zug machen!'></form>";
        ?>
        
        </div>
            </table>
        </div>
    </body>
</html>