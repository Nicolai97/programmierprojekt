<?php
   header("Content-type: text/css; charset: UTF-8");
   
   $fieldwidth = "60px"; 
   $fieldheight = "60px";	
   $whitefig = "#ff8000"; 				//Orange
   $blackfig = "#40ff00";				   //Grün
?>
body
{
	text-align: left;
	margin: auto;
	padding: auto;
	font-family: sans serif, Verdana;
	background-color: white;
	color: #0f81eb;

	

}



.Von_Nach
{
position: center;    //Position von Eingabefeld verändern
right: 0px;
width: 300px;
padding: auto;

}

span
{
background: yellow;
color:#000000;
}

div {
width: 800px;
margin: auto;
}


//th {	
//	border: 1px solid black;
//	padding: 50px;
//	font-size: 0.8em;
//}

td {
	border: 1px solid black;
	width: <?php echo $fieldwidth; ?>;
	height: <?php echo $fieldheight; ?>;
	font-size: 2.8em;
}

.white {
	color: <?php echo $whitefig; ?>
}

.black {
	color: <?php echo $blackfig; ?>
}

tr:nth-child(even) td:nth-child(odd),tr:nth-child(odd) td:nth-child(even) {
	background-color:white;
}

tr:nth-child(even) td:nth-child(even),tr:nth-child(odd) td:nth-child(odd) {
	background-color: black;
}

tr:first-child td:nth-child(n),tr:last-child td:nth-child(n),
tr:nth-child(n) td:first-child,tr:nth-child(n) td:last-child
{
		border: 1px solid black;
		padding: 5px;
		font-size: 2em;
		font-weight: normal;
		width: 12px;
		height: 12px;
		background-color: #a0a0a0;
}

a{
color:black;
text-decoration:none;
}
a:visited{							
color:black;
text-decoration: none;
}
a:active{
color:black;
text-decoration: none;
}
a:hover{								
color:black;
text-decoration:underline;
}
a:focus{
color: red;
text-decoration: none;
}
